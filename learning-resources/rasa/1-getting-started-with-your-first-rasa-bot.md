# Part 1 - getting started with your first Rasa bot

## Introduction
Rasa is an open source chatbot framework to create and automate conversational applications. It provides several features to make you develop your bot in easy way. With Rasa, you can generate your training data, train your model, run tests, and connect your bot to APIs and different messaging channels. 

## Prerequisites
Don't take the following prerequisites too seriously, but it is preferable to know how to use:
* Terminal, to move around your computer's hard drive.
* Git fundamentals (I'm kidding, you need to run only one command to retrieve Rasa open source code).

## Building from source
Because you are a software engineer, it is preferable to use the development version of Rasa open source. You can follow the instructions below to setup Rasa properly in your local machine.

### Create a conda environment
First, let's create a new conda environment based on Python 3.7 because Rasa and Tensorflow are more compatible with this particular version according to [Rasa Documentation](https://rasa.com/docs/rasa/). Then, we can use poetry package manager to install the dependencies needed to run Rasa properly.

```console
$ conda create -n rasaenv 'python>=3.7,<3.8' poetry
```
Note that you must activate your new conda environment to install Rasa and its dependencies in the right place. To activate a conda environment, use the following command:

```console
$ conda activate rasaenv
```

### Clone the Rasa source code
Create a new directory in your local hard drive where you can clone the Rasa source code. From your terminal, don't forget to go to the directory that was just created:

```console
$ mkdir ~/code
$ cd ~/code
$ git clone git@github.com:rasahq/rasa
$ cd ~/code/rasa 
```

### Run the peotry package manager
To install rasa in the right environment, make sure your conda environment is activated to run the peotry package manager. Poetry comes with a bunch of python packages like Tensorflow that Rasa needs for training.

```console
$ poetry install
```

Note that some machine learning algorithms require additional python packages to be installed, this [page](https://rasa.com/docs/rasa/installation) on Rasa documentation can help you configure those dependencies.

## Create a Rasa project
Any Rasa project contains some YAML files that forms the basis of skeleton conversation design of any bot. So, you can create a new directory in where you cloned the Rasa source code to hold your Rasa chatbot projects:

```console
$ mkdir ~/code/rasa-projects
$ cd ~/code/rasa-projects
```

To initialize your Rasa project, the `rasa init` command comes with a list of questions to enter a path for your project, and train your intial model. After you run the mentioned command, give a name to your project at the first prompt so Rasa can create a structure for your chatbot. Then, you can accept all the prompts except the lastest one in case you don't want to run your bot at the moment.

```console
? Please enter a path where the project will be created [default: current directory] my-first-rasa-chatbot                   
? Path '/home/code/rasa-projects/your-first-rasa-chatbot' does not exist 🧐. Create path?  Yes 

Created project directory at '/home/code/rasa-projects/my-first-rasa-chatbot'.
Finished creating project structure.
? Do you want to train an initial model? 💪🏽  Yes                                                                             
Training an initial model...
The configuration for policies and pipeline was chosen automatically. It was written into the config file at '/home/code/rasa-projects/my-first-rasa-chatbot/config.yml'.
Training NLU model...
...
Core model training completed.
Your Rasa model is trained and saved at '~/code/rasa-projects/my-first-rasa-chatbot/models/20220121-114355-burgundy-moat.tar.gz'.

? Do you want to speak to the trained assistant on the command line? 🤖  (Y/n) No

Ok 👍🏼. If you want to speak to the assistant, run 'rasa shell' at any time inside the project directory.
```

### Start with Rasa playground data
When you run the `run init` command, Rasa created a project structure for your bot that includes some dependency files like **config.yml** and some directories includes **data** and **models**. So, let's create a new YAML inside **data** directory, and name it something like **my-first-training-data.yml**. 

```console
$ cd ~/code/rasa-projects/my-first-rasa-bot/data
$ touch my-first-training-data.yml
```

Rasa has a nice conversation design that can get you start creating the first training data for your bot. You can go to [Rasa Documentation Playground](https://rasa.com/docs/rasa/playground) to build your assistant bot with an interactive guide. To keep things simple at the moment, let's copy the content of this [file](https://gitlab.com/rochdikhalid/blog/-/blob/main/data/training-data.yml) immediately into your training YAML file. Then, don't forget to import your intents in the **domain.yml** as we did [here](https://gitlab.com/rochdikhalid/blog/-/blob/main/data/domain.yml)

### Train your bot
Next, to run your first model using the training data you just created, you can use `rasa train` command that comes with two attributes. The `--data` used to enter the path where the training data is located, and the `--config` to define the **config.yml** that your model will use to make predictions based on user inputs.

```console
$ cd ..
$ rasa train --data data/my-first-training-data.yml --config config.yml
```
The training command above will help your Rasa bot classify what user is saying into the right intents, in a way your bot will be able to recognize different user inputs even with words that are not included into your training data file. The training process will ended up by saving your model to the **models** directory of your project.

### Run your bot
Lastly, you can run your first Rasa bot using the `rasa shell` command. Try to test manually your bot by typing some inputs that are not the same as you typed into the training YAML file. Feel free to stop the conversation at anytime you want using the `/stop` command.

## What's next?
I hope this tutorial could help you build your first basic Rasa bot. In the next parts, we will take a closer at the structure that Rasa created for our first chatbot to understand each component and what is used for. Later in this serie, we will confine our bot to a specific topic and write some tests to test the accuracy of our bot.
