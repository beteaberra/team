# Regenerative Business and how it relates to Prosocial AI

## My Latest Favorite "Aligned Businesses"

1. [Wikipedia](https://www.wikipedia.org)'s [Python API](https://gitlab.com/tangibleai/Wikipedia)
3. [GitLab](https://gitlab.com)
2. [Papers With Code](https://paperswithcode.com)
4. [Telegram](https://www.telegram.org)
5. [Mastodon](https://mastodon.social)
6. [EdEx](https://edex.org)
7. [Free Code Camp](https://www.freecodecamp.org)
8. [Simplicable](https://simplicable.com)
9. [Tutanota](https://tutanota.com)
10. [Next Cloud](https://nextcloud.org)


## AI

* [Moral Machine](https://www.moralmachine.net/)
* [Robust NLP](https://robustnlp-tutorial.github.io/)
* [DynaBoard (robust NLP)](https://dynabench.org/tasks/sentiment)
* [HuggingFace Gradio](https://github.com/gradio-app/gradio)
* [comma.ai](https://comma.ai)

### Navigation

* [OSM.org](openstreetmap.org)

### Data labeling

* [DynaBoard (robust NLP)](https://dynabench.org/tasks/sentiment)
* [MIT Moral Machine for SDCs](moralmachine.net)
* [HuggingFace Gradio](https://github.com/gradio-app/gradio)

## Presentations

* [DrawIO Web](https://github.com/jgraph/drawio) & [Desktop](https://github.com/jgraph/drawio-desktop)
* [LibreOffice]
* [NextCloud]

## Hardware

* [Pine Phone](https://www.pine64.org/)

### Mobile

* [PinePhone](https://www.pine64.org/)
* [Raspberry Pi](https://www.raspberrypi.org/)

### Desktop/Laptop

* [Foundation]

## Web Search

* [Quant](https://www.qwant.com/)
* [SearX](https://searx.thegpm.org/)
* [DISROOT](https://disroot.org)

### Document Search

* [Meilisearch](https://github.com/meilisearch/meilisearch)
* [SearX](https://github.com/searx/searx)
* [Apache Solr](https://solr.apache.org/)
* [Apache Lucene](https://lucene.apache.org/)
* [ElasticSearch](https://github.com/elastic/elasticsearch)
* [Pynn Descemnt]
* [Annoy]


## OS

* [Tizen](https://www.tizen.org/)
* [Raspbian]
* [PineOS]
* [Mobian]
* [13 FOSS Smartphone OSes](https://itsfoss.com/open-source-alternatives-android/)

## Education

* [Wikipedia](https://www.wikipedia.org)
* [Papers With Code](https://paperswithcode.com)
* [EdEx](https://edex.org)
* [Free Code Camp](https://www.freecodecamp.org)
* [Simplicable](https://simplicable.com)

## People

* [Maggie Kane](https://causeartist.com/pay-what-you-can-cafe-place-at-the-table-future-restaurant-industry/)

## Businesses

* [Place at the Table](https://tableraleigh.org/)
* [It's FOSS](https://itsfoss.com/)
* [elastic](https://www.elastic.co/elasticsearch/)
* [Canonical](https://ubuntu.com/)
* [Wikipedia](https://www.wikipedia.org)
* [FSF]
* [PSF]
* [Let's Encrypt]
* [Meilisearch](https://www.meilisearch.com/)


## Education

* [Wikipedia](https://www.wikipedia.org)
* [Papers With Code](https://paperswithcode.com)

## Software

* [Python](Python.org)
* [PSF]

## Lists

* [Open Source Smartphone OSes](https://itsfoss.com/open-source-alternatives-android/)
* [5.5 M Awesome Open Source Projects](https://awesomeopensource.com/)
* [](https://github.com/awesomedata/awesome-public-datasets) 

## TODO

- [ ] understand [this article](https://en.wikipedia.org/wiki/Sustainable_business#cite_note-44)
- [ ] understand [this article](https://en.wikipedia.org/wiki/Sustainable_business#cite_note-45) 
- [ ] add 2 specific business models to the [Sustainable_business article on wikipedia](https://en.wikipedia.org/wiki/Sustainable_business#Circular_business_models)
- [ ] Eda & Jessa: research Sustainable brands live event in San Diego, conf in October!

## Biggest Competitive

- circular economy: A circular economy (also referred to as "circularity"[2]) is an economic system that tackles global challenges like climate change, biodiversity loss, waste, and pollution. 
- regenerative business: 

## NextSD resources

- [Next SD Vison - POLARIS by Jessa Spainhower and Isabel Mo](https://www.canva.com/design/DAEjlsU7Uf8/3Qf0CdDOT4qYh3lxEvSjkQ/view#2)
- [Resilience & Regeneration](./Resilience & Regeneration.pdf)
- [Towards Regenerative Development by Richard McDonald](./Thesis - Towards Regenerative Development by Richard McDonald.doc)
- [2015-Regenerative Capitalism](./2015-Regenerative-Capitalism-4-20-15-final.pdf)

## Questions
- Is "Regenerative Business" and economic theory, like "Doughnut Economics"?
- Or is it more of a philosophy or ethic or way of thinking, like "Prosocial Business" or "Authentic Business"
- How is the term used differently from other terms which are already wikipedia article titles: "Regenerative Capitalism", "Regenerative Economic Theory", etc.
- Is regenerative business a widely used term? 
- What does it mean?
- Should we start a Wikipedia article on it?  Are there synonym pages or ambiguity redirect or disambiguation pages that we could add to?
- What are the most popular search results for "Regenerative Business"? (Who is doing SEO on the term.)
- Are there any for-profit companies actively promoting the term and using it to market their products?
- Do any big tech companies "take a stand" on the concept?

## Related Wikipedia pages

### closest synonyms
- [Regenerative economic theory](https://en.wikipedia.org/wiki/Regenerative_economic_theory) only 2 paragraphs written in 1999
    - rename to Regenerative economics?
    - consolidate with "Regenerative capitalism"
- [Regenerative capitalism](https://en.wikipedia.org/w/index.php?title=Regenerative_capitalism&redirect=no) redirects to [John B. Fullerton](https://en.wikipedia.org/wiki/John_B._Fullerton)
- [Regenerative economic theory](https://en.wikipedia.org/wiki/Regenerative_economic_theory)
- [Regeneration_(biology)](https://en.wikipedia.org/wiki/Regeneration_(biology))

### Wikipedia "Regeneration" disambiguation

- [Regeneration (biology)](https://en.wikipedia.org/wiki/Regeneration_(biology) "Regeneration (biology)"), the ability to recreate lost or damaged cells, tissues, organs and limbs
-   [Regeneration (ecology)](https://en.wikipedia.org/wiki/Regeneration_(ecology) "Regeneration (ecology)"), the ability of ecosystems to regenerate biomass, using photosynthesis
-   [Regeneration in humans](https://en.wikipedia.org/wiki/Regeneration_in_humans "Regeneration in humans"), the ability of humans to recreate, or induce the regeneration of, lost tissue
-   [Regenerative (design)](https://en.wikipedia.org/wiki/Regenerative_(design) "Regenerative (design)"), a process for resilient and sustainable development
-   [Regenerative agriculture](https://en.wikipedia.org/wiki/Regenerative_agriculture "Regenerative agriculture"), a sub-category of organic agriculture

## Terminology


### "Regenerative Business" related terms

- Inclusive Capitalism
- Sustainable Capitalism
- Stakeholder Capitalism//Capitalists
- Regenerative Capitalism/Capitalists 
- Regenerative 
- Conscious Capitalism (John Mackey, Raj Sisodia, Trademarked)
- Firms of Endearment 
- [authentic marketing](https://www.georgekao.com/authentic-marketing.html)
- authentic leadership
- Supercooperators

- Sustainable business
- ESG (Environmental, Social, Governance)
- prosocial business


### Related technologies

- Zero sum game
- Mechanism design
- prosocial algorithms
- beneficial AI
- human compatible AI

## Resources

- [World Economic Forum explanation of Regenerative Business](https://www.weforum.org/agenda/2020/01/the-regenerative-business-approach-a-roadmap-for-rapid-change/)
- Capital Institute (by Carol Sanford?)
- [Carol Sanford](https://carolsanford.medium.com/ )
- [Dr. Tamsen - living systems]()

## Examples

- Malala - GirlUp summit
- Article 22 and Opolis Optics 
- Zeitgeist San Diego

## 4 week program in Regenerative Business

- presentation to Bogota
- 
