# Week 3: Git from the Command Line

#### _`date_scheduled = datetime(2021, 2, 3, 30, 0)`_

#### _`title = 'scheduled'`_

For Week 3 you are going to learn how to use git from the command line.
You've already tried out the GUI at gitlab.com, but now you're going to learn how the professionals do it.
This will build on the shell commands that you already know (`cd`, `ls`, `rm`, `mkdir`, `nano`, and `more`).

You can refresh your memory on navigating directory structures and file paths with `bash` [here](../learning-resources/bash/README.md)
