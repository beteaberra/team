# Getting into Bottomline Server

1. Create ssh key for the VPN you will be using.
    - `ssh-keygen` in terminal
      - First prompt will ask for the path of the key. Save the key in a folder with your other ssh keys. If you do not have a folder, it is generally `~/.ssh`. You will name the key here. It is good practice to use a suffix to the type of key it is. In this case, we have an `rsa`, so for the file path I used: 
      `Users/billyhorn/.ssh/id_rsa_name_here`
      - DO NOT PUT IN PASSPHRASE ON ANY SSH KEYGEN
2. Edit ssh config:
    - Edit config file in terminal:
      `nano ~/.ssh/config`
    - Add information of host to config file. Modify the following lines to suit your need and append at the bottom of the config file.
      ```
      # the 'dl' here is the alias you will use when ssh into VPN.
      Host dl 

        # the path of the key created in Step 1.
        IdentityFile /Users/billyhorn/.ssh/id_rsa_name

        # Host name/address of VPN
        Hostname 10.49.0.3

        # Username on VPN
        User deeplearner

        # This is forwarding the X11 display so you can view the plot images on your local machine.
        ForwardX11 yes
      ```
3. Copy ssh ID to server. This will make it so you will not have to enter a passphrase everytime you access the server.

    - `ssh-copy-id dl`
    The `dl` is the alias of the server created in Step 2. (Here dl stands for deeplearner, the username on the server). For sake of verbosity, you can also follow the format:
    `ssh-copy-id user@hostname`
    - You will be prompted for a password. Enter the user password you would use to login to the server.

4. Save WireGuard config file provider by VPN host in a folder on your machine.

5. Download and install Wireguard application (see [link](https://www.wireguard.com/install/)).

6. Activate VPN:
    - Open WireGuard and add the WireGuard config file from Step 4.
    - There is an alarming message that pops up about activity being monitored by VPN. This is a standard message that most VPNs show and, although oddly worded, is basically stating that even though you are using a VPN your activity can still be monitored, in this case by the VPN and not your ISP.
    - Activate WireGuard tunnel.

 7. ssh into server!
     - From terminal:
     `ssh -Y dl`
     Here `-Y` enables trusted X11 forwarding. You could also try `-X`, but it didn't work for me ([link](https://unix.stackexchange.com/questions/12755/how-to-forward-x-over-ssh-to-run-graphics-applications-remotely)) 

# Bonus: tmux

Use tmux to spin up multiple programs in terminal. It will leave programs running in background in case you get knocked off server. It will also allow you to remotely pair-program with other users on the VPN.

- `tmux`: Starts terminal tab (tab is shown in green bar at bottom of screen, confusing - I know)

- `tmux a` or `tmux attach`: Attaches to existing intance of tmux

**Once isnside of a tmux program:**

- `Ctrl+b` then type `c` and press enter: This creates a new tab.

- `Ctrl+b` then type `n`: Opens next tab

- `Ctrl+b` then type `d`: Detaches from current tmux instance.

- `Ctrl+b` then type X: This gives you option to kill the current tmux instance (y/n).

- `htop` entered in the terminal window within tmux: Thhis will show server statistics.


