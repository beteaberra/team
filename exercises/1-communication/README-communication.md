# Communication

Verbal and written communication is the cornerstones of being a human.
And it's the foundaction for society and culture.
Individual communication is the firing of "neurons" in the collective consciousness that creates society.
If it goes poorly, it's like the neurons in our brain misfiring.
It gives us a headache.
We collectively feel the pain of poor communication in the post-truth era when most communication that we see online is driven by profit motive and greed.

## Resources

- [workplace games](https://positivepsychology.com/communication-exercises-for-work/) that can improve you communication skills.
- [How to Have Impossible Conversations]() by Peter Boghossian
- [Grice's Maxims of effective communication](https://effectiviology.com/principles-of-effective-communication/)
- [Rapoport's Rules](https://www.rightattitudes.com/2017/06/16/rapoport-rules-criticism/)
