# Alcohol Consumption

## References

- "Dose-related effects of red wine and alcohol on hemodynamics, sympathetic nerve activity, and arterial diameter"[^1] by Jonas Spaak, et al.
- Data Science Lesson for UCSD DSDH


- Thirteen volunteers (24-47 yr; 7 men, 6 women)


- bal_per_drink_wine_mean = 38
- bal_per_drink_wine_std = 2
- bal_per_drink_ethanol_mean = 39
- bal_per_drink_ethanol_std = 2
- bal_per_2drinks_wine_mean = 72
- bal_per_2drinks_wine_std = 4
- bal_per_2drinks_ethanol_mean = 83
- bal_per_2drinks_ethanol_std = 3



[^1] [Dose-related effects of red wine and alcohol on ...](https://pubmed.ncbi.nlm.nih.gov/18055508/) "...hemodynamics, sympathetic nerve activity, and arterial diameter"
by Jonas Spaak, Anthony C Merlocco, George J Soleas, George Tomlinson, Beverley L Morris, Peter Picton, Catherine F Notarius, Christopher T Chan, John S Floras
