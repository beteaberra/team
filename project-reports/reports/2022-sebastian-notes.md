# Sebastian's Notes
Here is the file that I will keep the notes I make every week to keep track of my progress on my Tangible AI internship

Project ideas:
some sorta chat bot
grammar rater chatbot
text paraphraser
Google AI4code kaggle competition

Text paraphraser:
user inputs text > encoder > decoder
the idea is that the decoder decodes the encoded input into a sentence that hopefully means the same thing but differs from the user input. My idea is that the loss function could be some pretrained encoder model, that encodes the output and input. The difference between the encodings is found and used with the difference in letters from the output and input.
Or just use a normal model with the same idea for loss.
Once I build a working model and can turn it into a chatbot type thing and maybe add a website.

Google AI4code:
just build a model to to the task, not so exited about this one as I can't think of anyway to add onto it if I finish it.

Grammar chatbot:
not such an orginal idea, just make chatbot that rates grammr

Some sorta chatbot:
just make some sorta chatbot

Loss function ideas for text paraphrases:
Embedding model encodes output and input > difference measured between the two
The above is used in some combination with the difference in characters of the input and output.

`'bert-base-nli-mean-tokens` from the sentence_transformers package can be used to encode english text. The cosine similarity can then be measured with `sklearn.metrics.pairwise.cosine_similarity`. (https://towardsdatascience.com/bert-for-measuring-text-similarity-eec91c6bf9e1)
The `paraphrase-multilingual-mpnet-base-v2` pretrained model can also be used and seems to perform better than other models.
Instead of `sklearn.metrics.pairwise.cosine_similarity` can `utila.cos_sim` can also be used from the sentence_transformers package.

Different chatbot ideas:
Grammar chatbot
Chatbot that knows about certain topics

tabular data sets:
stackoverflow dataset
ai4code dataset
paraphrase dataset

Total score is Fun + Impact / Difficulty
Fun, Impact, and Difficulty are all scored out of three
Text paraphraser:
Dataset: https://huggingface.co/datasets/paws
+ Fun: 3
+ Impact: 2
- Difficulty: 2
* Total score: 2.5
Reference: https://aclanthology.org/D19-5627/ https://www.tensorflow.org/text/tutorials/transformer

Google AI4code:
Dataset: https://www.kaggle.com/competitions/AI4Code/data
+ Fun: 2
+ Impact: 1
- Difficulty: 3
* Total Score: 1
Reference: https://www.kaggle.com/code/fmakarem/ai4code-eda

Chatbot:
Dataset: https://www.kaggle.com/datasets/stackoverflow/stacksample?select=Questions.csv
Fun: 3
Impact: 2
Difficulty: 3
Total Score: 1.6
Reference:
