# Rasa dialog trees without the trees

## Introduction
Rasa is an open-source chatbot framework to create and automate conversational applications. It provides several features to make you efficiently develop your bot. With Rasa, you can generate your training data, train your model, run tests, and connect your bot to APIs and different messaging channels.

## How does Rasa framework work?
Rasa framework contains two libraries, **Rasa Core** and **Rasa NLU**. The first library represents the natural language understanding of the framework. It can be the interpreter that classifies intents and extracts entities based on machine learning techniques to make the bot able to understand the user inputs. The second library comes to manage and maintain the conversation state using the following parameters to execute the action needed and send a message to the user:

* **Tracker** is used to keep track of the conversation history and save it in memory. 
* **Policy** is the decision-maker of Rasa, it decides on what action the bot should take at every step in the conversation.
* **Featurizer** with the help of **Tracker**, it generates the vector representation of the current conversation state so the action can pass later a **Tracker** instance after every interaction with the user.

Because Rasa can create a dialog, it just doesn't use dialog trees. The chart below shows an example of a real chatbot project in progress. This bot focuses on helping people tackle their depression thoughts and boost their mental health condition. Rasa depends on one of the most interesting concepts in computer science named **FSM (Finite State Machine)** to describe how things work. Here is an elementary definition to understand how **FSM** operates:

* It is a computational model that consists of one or more states.
* It can be only in a single active state at any given time.
* An input to the machine causes a transition to the next state.
* Each state defines the state to switch to based on the user input.

![alt text](diagram-tree-example-uncolored.png "Rasa Basic Diagram")

The diagram above shows an example of an FSM. Here, shapes represent the machine (bot) states, this machine starts with an initiator state **OPEN** and ends with a terminator state **END**. The _Get to know some depression statistics_ is an example of inputs a user can enter to change any current state, meaning that this input defines the state to switch to so the machine can make a transition to the next state. However, it is possible to see sometimes an input that can keep the machine running in the same state. Let's take an example of a light bulb. This object has two states, it's either turned on or turned off at any given time. If we turn on the bulb, this can cause a transition from turned off to turned on and vice versa. **ON** state defines the state to switch to which is **OFF**, and **OFF** state defines the state to switch to which is **ON**. In other words, if the current state is **ON**, clicking on **ON** changes nothing, but pushing **OFF** changes the state to **OFF**. If we are in state **OFF**, pressing **OFF** keeps the machine in the same state, but pushing **ON** makes a transition to state **ON**.

## Conversation flow
In Rasa, **Stories** represents and manages the conversation flow between the user and the bot. Here, the user inputs are defined as **intents** which are just the identifiers you defined in the training data. The bot responses are defined as **actions**. In other words, **Stories** define the dialog _tree_. The dialog _tree_ is actually a _graph_ - connections between bot states. Each state is something the bot will do or say. The edges or transitions between states are the actions or statements the user makes.  So the **stories** create the design of the conversation that determines the steps of the entire interaction.

## NLU and Intent classification
NLU can define the training data and the example of utterances to categorize what the user wants to achieve during an interaction with a bot. The purpose of using NLU is to identify and extract the structured information from user inputs that can include **intents**, **entities**, and other extra information like **regular expressions** to improve the bot performance. Rasa gives you the freedom to define the NLU training data by classifying the user utterances into categories that represent **intents**. **Intents** is the skeleton of NLU to describe what users might say in each category, you can give them any name you want but it is preferable for each intent to have a name that matches the goal users like to accomplish with that **intent**.

## Entity recognition
Term **ENTITIES** belongs to a big area of research in NLP named **NER** which stands for **N**amed **E**ntity **R**ecognition. Researchers and professors use the academic term **ENTITIES** to refer to a particular object in the world (person, animal, place, thing, and even concept). In Rasa, **Entities** are structured pieces of information that can be extracted from user messages, it can hold important detail about the user like numbers, dates, names so that the bot could use them later in the conversation. Let's take an example for a flight booking, it would be very useful for your bot to know which detail in user input refers to a **destination**.

Also, there are other ways **entities** can be extracted in Rasa:

* **Synonyms** can be used in case users may refer to the information the bot wants to extract in multiple ways, so the bot can map the extracted **entities** to a single value.

* **Lookup tables** used to extract words users may say that refer to the **entities** you set in your training examples.

* **Regular expressions** helps you to assign a pattern to an entity name to improve the extraction.

Note that before you decide whether you should use **entities** or not, it's preferable to think about what information the bot needs for the user goals.

## Useful features
Rasa has some other useful features that can help to manage the conversation state and improve the bot performance. For example, **Tracker Store** keeps track of your conversation history as you interact with your bot, the conversation will be stored in the memory or SQL database. Another feature that can be important for evaluating your bot called **Markers**. This feature is used to express the important events in dialogs and mark them when they occurred. It helps to evaluate the bot performance based on how those specific events play out in a conversation. In other words, **Markers** are kind of conditions that allow you to mark the points of interest in your dialog to evaluate your bot. As some code examples highlighted in this [blog](https://rasa.com/blog/markers-in-rasa-open-source-3-0/), you can create an independent story with Markers and you can set up a **Tracker Store** along with to keep track of the conversation and export the **Markers**.

## Common problems
However, some features in Rasa can cause some rebundancy and confusion while improving the bot performance. For example, the **entity extraction** feature is useful for filling forms or getting information from the user, but the feature is not designed to make if and else decisions to create forks in the dialog tree. The **lookup table** doesn't work properly when the user puts in a truly "unseen" item, an item that you didn't put into the **lookup table**. On the other hand, **intents** in Rasa are callable at all times and the **checkpoints** which used to reduce the rebundancy can make the conversation flow hard to understand and make the bot missed everything up in addition to the **unfriendly documentation** that can be very difficult for newbies to go along with and even hard sometimes for developers to understand.

## Conclusion
To conclude, in this blog post we have introduced Rasa, which provides a platform to create and maintain conversational chatbots. We showed how Rasa operates in the background, and then demonstrated how this framework designs the dialog without any need to dialog trees. The blog highlighted some strengths and weaknesses Rasa has in terms of intent classification and entity extraction with a particular focus on some features that might have some future customization to improve more the framework performance.












